const { LanguageClient, TransportKind } = require('vscode-languageclient/node');

let client;

exports.activate = extension =>
{
	const serverModule = extension.asAbsolutePath("server.js");

	const serverOptions =
	{
		run: { module: serverModule, transport: TransportKind.ipc },
		debug: {	module: serverModule, transport: TransportKind.ipc, options: { execArgv: ['--nolazy', '--inspect=6009'] } }
	};

	const clientOptions =
	{
		documentSelector: [{ scheme: 'file', language: 'javascript' }],
	};

	client = new LanguageClient('rst', 'RST', serverOptions, clientOptions );

	client.start();
}

exports.deactivate = () => client?.stop();
