const { createConnection, TextDocuments, DiagnosticSeverity:{Information, Error}, ProposedFeatures, TextDocumentSyncKind: {Incremental} } = require('vscode-languageserver/node');
const { TextDocument } = require('vscode-languageserver-textdocument');
const Parser = require("web-tree-sitter");
const path = require("path");

const connection = createConnection(ProposedFeatures.all);
const documents = new TextDocuments(TextDocument);
connection.listen();
documents.listen(connection);

let parser;
let functions;
let objects;
let scopes = [];
let scope = {};
let diagnostics = [];
let typedNodes = new Set();
let uri;
let current_switch_expression;

connection.onInitialize( async client =>
{
	await Parser.init();
	const JS = await Parser.Language.load(path.join(__dirname,"tree-sitter-javascript.wasm"));
	parser = new Parser();
	parser.setLanguage(JS);

	return { capabilities: { textDocumentSync: Incremental } };
});

documents.onDidChangeContent( change =>
{
	reset();

	const document = change.document;
	uri = document._uri;

	const tree = parser.parse(document._content);
	const rootNode = tree.rootNode;

	buildTypeTree(rootNode);
	buildReportTree(uri);

	connection.sendDiagnostics({uri, diagnostics});
});


function reset()
{
	functions = {};
	objects = {};
	scope = {};
	scopes = [];
	diagnostics = [];
	typedNodes = new Set();
}

function buildTypeTree(node)
{
	switch (node.type)
	{
		case "object":
		{
			const fields = {};
			for(let i = 1; i < node.childCount; i+=2)
			{
				const field = node.children[i];
				const [name, ,expression] = field.children;
				fields[name.text] = expression;
				relate(name,expression,"object definition");
			}
			node.fields = fields;
		}
		break;
		case "member_expression":
		{
			const [object, ,member] = node.children;
			if (objects[object.text]===undefined)
			{
				addDiagnostic(uri, node, Error, "Inexistent object");
			}
			else if (objects[object.text].fields === undefined)
			{
				addDiagnostic(uri, node, Error, "Is not an object or empty object");
			}
			else if (objects[object.text].fields[member.text] === undefined)
			{
				addDiagnostic(uri, node, Error, "Object has no field named "+member.text);
			}
			else
			{
				relate(node,objects[object.text].fields[member.text], "object member");
			}
		}
		break;
		case "function_declaration":
		{
			const [,identifier,parameters] = node.children;
			type(identifier, "function", "function declaration");
			const parameterNodes = [];
			for (let i = 1; i < parameters.childCount - 1; i+= 2)
			{
				parameterNodes.push(parameters.children[i]);
			}
			functions[identifier.text] = parameterNodes;
		}
		break;
		case "call_expression":
		{
			const [identifier, arguments] = node.children;
			type(identifier, "function", "call", []);
			const argumentNodes = [];
			for (let i = 1; i < arguments.childCount - 1; i+= 2)
			{
				argumentNodes.push(arguments.children[i]);
			}
			if (functions[identifier.text] === undefined || functions[identifier.text].length != argumentNodes.length)
			{
				addDiagnostic(uri, identifier, Error, "Inexistent function", []);
			}
			else
			{
				for (let i = 0; i < argumentNodes.length; i++)
				{
					relate(functions[identifier.text][i], argumentNodes[i], "argument parameter");
				}
			}
		}
		break;
		case "{":
			scopes.push(Object.assign({}, scope));
		break;
		case "}":
			scope = scopes.pop();
		break;
		case "identifier": // TODO Clean up the mess
		{
			if (scope[node.text])
			{
				if (!node.parentNode)
				{
					relate(scope[node.text], node, "scope");
				}
				else if (!scope[node.text].parentNode)
				{
					relate(node, scope[node.text], "scope");
				}
				else
				{
					let selected = node;
					while (selected.parentNode)
					{
						selected = selected.parentNode;
					}
					relate(scope[node.text], selected, "transitive " + node.text);
				}
			}
			else
			{
				scope[node.text] = node;
			}
		}
		break;
		case "if_statement":
		{
			type(node.children[1].children[1], "boolean", "if condition", []);
		}
		break;
		case "while_statement":
		{
			type(node.children[1].children[1], "boolean", "while condition", []);
		}
		break;
		case "parenthesized_expression":
		{
			if (!["while_statement", "if_statement", "do_statement", "switch_statement"].includes(node.parent.type))
			{
				relate(node, node.children[1], "parenthesized expression");
			}
		}
		break;
		case "unary_expression":
		{
			const [operator,operand] = node.children;
			switch (operator.text)
			{
				case "!":
					type(node, "boolean", "negation result");
					type(operand, "boolean", "negation operand");
				break;
				case "~":
					type(node, "number", "bitwise negation result");
					type(operand, "number", "bitwise negation operand");
				break;
				case "+":
					type(node, "number", "unary plus result");
					type(operand, "number", "unary plus operand");
				break;
				case "-":
					type(node, "number", "unary minus result");
					type(operand, "number", "unary minus operand");
				break;
				case "typeof":
					type(node, "string", "typeof result");
				break;
				case "void":
					type(node, "undefined", "void result");
				break;
				case "delete":
					type(node, "boolean", "delete result");
				break;
			}
		}
		break;
		case "update_expression":
		{
			const [operator, operand] = node.children;
			switch (operator.text)
			{
				case "++":
					type(node, "number", "increment result");
					type(operand, "number", "increment operand");
				break;
				case "--":
					type(node, "number", "decrement result");
					type(operand, "number", "decrement operand");
				break;
			}
		}
		break;
		case "binary_expression":
		{
			const [left,operator,right] = node.children;
			switch (operator.text)
			{
				case "&&":
				case "||":
					type(node, "boolean", "boolean algebra result");
					type(left, "boolean", "boolean algebra operand");
					type(right, "boolean", "boolean algebra operand");
				break;
				case "<<":
				case ">>":
				case ">>>":
					type(node, "number", "shift result");
					type(left, "number", "shift operand");
					type(right, "number", "shift operand");
				break;
				case "&":
				case "|":
				case "^":
					type(node, "number", "bitwise result");
					type(left, "number", "bitwise operand");
					type(right, "number", "bitwise operand");
				break;
				case "<":
				case "<=":
				case ">":
				case ">=":
					type(node, "boolean", "comparison result");
					type(left, "number", "comparison operand");
					type(right, "number", "comparison operand");
				break;
				case "-":
				case "*":
				case "/":
				case "%":
				case "**":
					type(node, "number", "arithmetic result");
					type(left, "number", "arithmetic operand");
					type(right, "number", "arithmetic operand");
				break;
				case "+":
					relate(node, left, "homogeneous addition");
					relate(node, right, "homogeneous addition");
				break;
				case "??":
					relate(node, left, "homogeneous coalesce");
					relate(node, right, "homogeneous coalesce");
				break;
				case "instanceof":
					type(node, "boolean", "instanceof result");
					type(left, "object", "instanceof left");
					// TODO type right as contructor or class
				break;
				case "in":
					type(node, "boolean", "in result");
					type(left, "string", "in left");
					type(right, "object", "in right");
				break;
			}
		}
		break;
		case "ternary_expression":
		{
			const [condition, ,consequence, ,alternative] = node.children;
			type(condition, "boolean", "ternary condition");
			relate(node, consequence, "homogeneous ternary");
			relate(node, alternative, "homogeneous ternary");
		}
		break;
		case "assignment_expression":
		case "variable_declarator":
		{
			const [identifier, operator, expression] = node.children;

			if (expression)
			{
				relate(identifier, expression, "assignment");

				if (expression.type === "object")
				{
					objects[identifier.text]=expression;
				}
			}
		}
		break;
		case "augmented_assignment_expression":
		{
			const [identifier, operator, expression] = node.children;

			switch (operator.text)
			{
				case "-=":
				case "*=":
				case "/=":
				case "**=":
					type(node, "number", "arithmetic assignment result");
					type(identifier, "number", "arithmetic assignment");
					type(expression, "number", "arithmetic assignment");
				break;
				case "&=":
				case "|=":
				case "^=":
					type(node, "number", "bitwise assignment result");
					type(identifier, "number", "bitwise assignment");
					type(expression, "number", "bitwise assignment");
				break;
				case ">>=":
				case ">>>=":
				case "<<=":
					type(node, "number", "shift assignment result");
					type(identifier, "number", "shift assignment");
					type(expression, "number", "shift assignment");
				break;
				case "&&=":
				case "||=":
					type(node, "boolean", "boolean assignment result");
					type(identifier, "boolean", "boolean assignment");
					type(expression, "boolean", "boolean assignment");
				break;
				case "??=":
					relate(identifier, expression, "homogeneous coalesce assignment");
					relate(node, identifier, "homogeneous coalesce assignment");
				break;
				case "+=":
					relate(identifier, expression, "homogeneous addition");
					relate(node, identifier, "homogeneous addition");
				break;
			}
		}
		break;
		case "number":
			type(node, "number", "literal");
		break;
		case "string":
		case "template_string":
			type(node, "string", "literal");
		break;
		case "array":
			type(node, "array", "literal");
		break;
		case "false":
		case "true":
			type(node, "boolean", "literal");
		break;
		case "undefined":
			type(node, "undefined", "literal");
		break;
		case "null":
			type(node, "null", "literal");
		break;

		case "sequence_expression":
			// TODO
		break;
		case "do_statement":
		{
			const [,,,condition] = node.children;
			if (condition)
			{
				const [,expression] = condition.children;
				if (expression)
				{
					type(expression, "boolean", "do...while condition");
				}
			}
		}
		break;
		case "for_statement":
		{
			const [,,,condition] = node.children;
			if (condition)
			{
				const [expression] = condition.children;
				if (expression)
				{
					type(expression, "boolean", "for condition");
				}
			}
		}
		break;
		case "for_in_statement":
		{
			let offset = 0;
			if (node.children[2].type === "var")
			{
				offset = 1;
			}
			const element = node.children[2+offset];
			const object = node.children[4+offset];
			type(element, "string", "field of for...in");
			type(object, "object", "object of for...in");
		}
		break;
		case "switch_statement":
		{
			current_switch_expression = node.children[1].children[1];
		}
		break;
		case "switch_case":
		{
			const expression = node.children[1];
			relate(current_switch_expression, expression, "switch case");
		}
		break;
		break;
		case "return_statement":
		{
			// TODO important
		}
		break;
		case "with_statement":
		{
			// TODO not important
		}
		break;
		case "try_statement":
		case "catch_clause":
		case "finally_clause":

		case "program":
		case "switch_body":
		case "switch_default":
		case "expression_statement":
		case "variable_declaration":
		case "statement_block":
		case "empty_statement":
		case "else_clause":
		case "continue_statement":
		case "break_statement":
		case "debugger_statement":

		case "throw_statement":
			// ! may ruin the return type of functions, also bubbling, throws in catches

		case "labelled_statement": // ? labels for continue and break
		case "statement_identifier": // ? continue and break's label statement
			// TODO check for loop in scope

		case "formal_parameters":
		case "arguments":

		case "switch":
		case "case":
		case "default":
		case "function":
		case "return":
		case "throw":
		case "catch":
		case "finally":
		case "with":
		case "var":
		case "else":
		case "do":
		case "while":
		case "break":
		case "continue":
		case "(":
		case ")":
		case "{":
		case "}":
		case ",":
		case ";":
		case "=":
		case "\"":
		break;
	}
	// TODO shortcircuit if nothing interesting in the children
	for (const child of node.children || [])
	{
		buildTypeTree(child);
	}
}


function buildReportTree(uri)
{
	for (const node of typedNodes)
	{
		if (Object.keys(node.types).length > 1)
		{
			report(node, uri);
			break;
		}
		if(node.parentNode)
		{
			type(node.parentNode, Object.keys(node.types)[0], node.reason, Object.values(node.types)[0]);

			if (Object.keys(node.parentNode.types).length > 1)
			{
				report(node.parentNode, uri);
				break;
			}
		}
	}
}

function report(node, uri)
{
	const relatedInformation = [];

	let [firstType, secondType] = Object.keys(node.types);
	let [first,second] = Object.values(node.types);

	if (first[0].node.startPosition.row > second[0].node.startPosition.row)
	{
		[second,first] = [first,second];
		[secondType, firstType] = [firstType, secondType];
	}

	const records = [...first, ...second.reverse()];
	const firstRecord = records[0];
	const lastRecord = records[records.length-1];

	addDiagnostic(uri, firstRecord.node, Error, firstType + " " + firstRecord.message, relatedInformation);
	for (let i = 1; i < first.length; i++)
	{
		addDiagnostic(uri, records[i].node, Information, records[i].message, relatedInformation);
	}
	for (let i = 0; i < second.length - 2; i++)
	{
		addDiagnostic(uri, records[first.length+i+1].node, Information, records[first.length+i].message, relatedInformation);
	}
	addDiagnostic(uri, lastRecord.node, Error, secondType + " " + lastRecord.message, relatedInformation);

	if (records.length > 2)
	{
		diagnostics[0].message = "Start with " + diagnostics[0].message;
		relatedInformation[0].message = diagnostics[0].message;
		diagnostics[diagnostics.length-1].message = second[second.length-1].node.reason + " to " + diagnostics[diagnostics.length-1].message;
		relatedInformation[relatedInformation.length-1].message = diagnostics[diagnostics.length-1].message;

		//diagnostics = diagnostics.filter(x => x != diagnostics[diagnostics.length-2]);
	}
	else
	{
		diagnostics = [diagnostics[0]];
	}
}

function relate(node, related, reason)
{
	related.parentNode = node;
	related.reason = reason;
}

function type(node, type, reason, trace = [])
{
	typedNodes.add(node);
	if (node.types && node.types[type])
	{
	}
	else if (node.types)
	{
		node.types[type] = [...trace, {node:node, message:reason}];
	}
	else
	{
		node.types = {};
		node.types[type] = [...trace, {node:node, message:reason}];
	}
}

function addDiagnostic(uri, node, severity, message, relatedInformation=[])
{
	const diagnostic =
	{
		severity,
		message,
		relatedInformation,
		source: "rst",
		range: {start:{line:node.startPosition.row,character: node.startPosition.column}, end:{line:node.endPosition.row,character:node.endPosition.column}},
	};

	diagnostics.push(diagnostic);

	relatedInformation.push
	({
		location: { uri, range: diagnostic.range },
		message: diagnostic.message
	});
}
